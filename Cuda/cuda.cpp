#include "stdafx.h"
#include "bc.h"
#include <iostream>
#include "Windows.h"

extern "C"{
#include "aes_cuda.h"
	//#include "aes_cuda.cu"
	}

using namespace std;

unsigned long long startTimer()
	{
	LARGE_INTEGER start;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&start);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return start.QuadPart;
	}
unsigned long long endTimer()
	{
	LARGE_INTEGER stop;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&stop);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return stop.QuadPart;
	}

int main() {
	unsigned long long time;
	bool CUDA = true;

	if(!CUDA)
		{
		blockCipher bc;

		bc.loadPlain("ReadMe.txt.old"); //blockCipher jest okolo 12 (!) razy nadmiarowy w stosunku do rozmiaru pliku (*2 przy obydwu operacjach)

		time = startTimer();
		bc.encryptCpuECB();
		cout << "Szyfrowanie CPU: " << endTimer() - time << '\n';

		time = startTimer();
		bc.decryptCpuECB();
		cout << "Deszyfrowanie CPU: " << endTimer() - time << '\n';
		bc.savePlain("ReadMe.txt.old");
		}
	else
		{
		AES_KEY key;
		int multiprocessors_count = 0;
		ifstream file("ReadMe.txt.old", ios::binary | ios::ate);
		size_t nbytes = file.tellg();
		file.seekg(file.beg);
		unsigned char* input = new unsigned char[nbytes];
		unsigned char* output = new unsigned char[nbytes];
		file.read((char *)input, nbytes);
		AES_cuda_init(&multiprocessors_count, 1024*1024*256, 1); //pierwsza wartosc nie ma wiekszego znaczenia
		//druga wartosc to rozmiar bufora - STANOWI GORNA GRANICE ROZMIARU
		//DANYCH, powinien byc okolo 2 razy mniejszy od RAM urzadzenia z CUDA
		//trzecia wartosc to okreslenie verbosity: 2>1>0
		AES_set_encrypt_key((unsigned char*)"9hab45ghc7d64hg7", 128, &key); //przygotowanie klucza
		AES_cuda_transfer_key(&key); //przeslanie PRZYGOTOWANEGO klucza do urzadzenia
		time=startTimer();

		size_t current=0;
		int chunk;
		while (nbytes!=current) {
			chunk=(nbytes-current)/(MAX_THREAD*STATE_THREAD);
			if(chunk>=1) {
				AES_cuda_encrypt((input+current),(output+current),chunk*MAX_THREAD*STATE_THREAD);
				current+=chunk*MAX_THREAD*STATE_THREAD;	
				} else {
					AES_cuda_encrypt((input+current),(output+current),(nbytes-current));
					current+=(nbytes-current);
				}
			}
		cout << "Szyfrowanie CUDA: " << endTimer() - time << '\n';
		//AES_cuda_finish();
		//AES_cuda_init(&multiprocessors_count, 1024*1024*256, 1);

		AES_set_decrypt_key((unsigned char*)"9hab45ghc7d64hg7", 128, &key);
		AES_cuda_transfer_key(&key); //przeslanie PRZYGOTOWANEGO klucza do urzadzenia

		time = startTimer();
		current=0;
		chunk=0;
		while (nbytes!=current) {
			chunk=(nbytes-current)/(MAX_THREAD*STATE_THREAD);
			if(chunk>=1) {
				AES_cuda_decrypt((output+current),(output+current),chunk*MAX_THREAD*STATE_THREAD);
				current+=chunk*MAX_THREAD*STATE_THREAD;	
				} else {
					AES_cuda_decrypt((output+current),(output+current),(nbytes-current));
					current+=(nbytes-current);
				}
			}
		cout << "Deszyfrowanie CUDA: " << endTimer() - time << '\n';
		AES_cuda_finish();
		//cout<< output <<'\n';

		/*AES_cuda_transfer_iv("ahsjckritl4869dg"); //iv 16 bajtowe

		current=0;
		chunk=0;
		while (nbytes!=current) {
		chunk=(nbytes-current)/(MAX_THREAD*STATE_THREAD);
		if(chunk>=1) {
		AES_cuda_encrypt_cbc((output+current),(output+current),chunk*MAX_THREAD*STATE_THREAD);
		current+=chunk*MAX_THREAD*STATE_THREAD;	
		} else {
		AES_cuda_encrypt_CBC((output+current),(output+current),(nbytes-current));
		current+=(nbytes-current);
		}
		}

		//AES_cuda_transfer_iv("ahsjckritl4869dg"); //iv 16 bajtowe
		current=0;
		chunk=0;
		while (nbytes!=current) {
		chunk=(nbytes-current)/(MAX_THREAD*STATE_THREAD);
		if(chunk>=1) {
		AES_cuda_decrypt_cbc((output+current),(output+current),chunk*MAX_THREAD*STATE_THREAD);
		current+=chunk*MAX_THREAD*STATE_THREAD;	
		} else {
		AES_cuda_decrypt_CBC((output+current),(output+current),(nbytes-current));
		current+=(nbytes-current);
		}
		}
		*/
		fstream wfile("ReadMe.txt.old", ios::binary);
		wfile.write((const char*) output, nbytes);
		delete input;
		delete output;
		}
	system("pause");
	return 0;
	}

