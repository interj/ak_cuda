//wydzielony .cpp, bo stadfx.h nie dzialal poprawnie

#include "stdafx.h"
#include "bc.h"


blockCipher::blockCipher() {

	AutoSeededRandomPool prng;
	key.resize(AES::DEFAULT_KEYLENGTH);
	prng.GenerateBlock( key, key.size() );
	iv.resize(AES::BLOCKSIZE);
	prng.GenerateBlock( iv, iv.size() );
}
blockCipher::~blockCipher() {
}

string blockCipher::encryptCTR( string &plain, bool padding = false ) {
	
	try {
		string cipher;

		CTR_Mode<AES>::Encryption e;
		e.SetKeyWithIV( key, key.size(), iv ); // automatyczne ustawianie klucza

		if(padding) {

			StringSource( plain, true, 
						  new StreamTransformationFilter( e,
						  new StringSink( cipher ),
						  StreamTransformationFilter::PKCS_PADDING //powinno dac sie ustawic padding dla ostatniego 
														) // StreamTransformationFilter
						); // StringSource	
						
			return cipher;
		} else {

			StringSource( plain, true, 
						  new StreamTransformationFilter( e,
						  new StringSink( cipher ),
						  StreamTransformationFilter::NO_PADDING //powinno dac sie ustawic padding dla ostatniego 
														) // StreamTransformationFilter
						); // StringSource	
						
			return cipher;
		}
	} catch( CryptoPP::Exception& e ) {

		cerr << e.what() << endl;
		exit(1);
	}
}
string blockCipher::decryptCTR( string &cipher, bool padding = false) {

	try {
		string recovered;
		
		CTR_Mode<AES>::Decryption e;
		e.SetKey( key, key.size() );
		if(padding) {

			StringSource( cipher , true, 
						  new StreamTransformationFilter( e,
						  new StringSink( recovered ),
						  StreamTransformationFilter::PKCS_PADDING
														) // StreamTransformationFilter
						); // StringSource
			return recovered;
		} else {

			StringSource( cipher , true, 
						  new StreamTransformationFilter( e,
						  new StringSink( recovered ),
						  StreamTransformationFilter::NO_PADDING
														) // StreamTransformationFilter
						); // StringSource
			return recovered;
		}
	} catch( CryptoPP::Exception& e ) {

		cerr << e.what() << endl;
		exit(1);
	}
}

string blockCipher::encryptECB( string &plain, bool padding = false) {
	
	try {
		string cipher;

		ECB_Mode<AES>::Encryption e;
		e.SetKey( key, key.size() ); // automatyczne ustawianie klucza

		if(padding) {

			StringSource( plain, true, 
						  new StreamTransformationFilter( e,
						  new StringSink( cipher ),
						  StreamTransformationFilter::PKCS_PADDING //powinno dac sie ustawic padding dla ostatniego 
														) // StreamTransformationFilter
						); // StringSource	
						
			return cipher;
		} else {

			StringSource( plain, true, 
						  new StreamTransformationFilter( e,
						  new StringSink( cipher ),
						  StreamTransformationFilter::NO_PADDING //powinno dac sie ustawic padding dla ostatniego 
														) // StreamTransformationFilter
						); // StringSource	
						
			return cipher;
		}
	} catch( CryptoPP::Exception& e ) {

		cerr << e.what() << endl;
		exit(1);
	}
}
string blockCipher::decryptECB( string &cipher, bool padding = false ) {
		
	try {
		string recovered;
		
		ECB_Mode<AES>::Decryption e;
		e.SetKey( key, key.size() );
		if(padding) {

			StringSource( cipher , true, 
						  new StreamTransformationFilter( e,
						  new StringSink( recovered ),
						  StreamTransformationFilter::PKCS_PADDING
														) // StreamTransformationFilter
						); // StringSource
			return recovered;
		} else {

			StringSource( cipher , true, 
						  new StreamTransformationFilter( e,
						  new StringSink( recovered ),
						  StreamTransformationFilter::NO_PADDING
														) // StreamTransformationFilter
						); // StringSource
			return recovered;
		}
	} catch( CryptoPP::Exception& e ) {

		cerr << e.what() << endl;
		exit(1);
	}
}

void blockCipher::encryptCpuCTR() {

	cipher.text.clear();
	list<string>::iterator it = plain.text.begin();
	while(it != plain.text.end()) {

		cipher.text.push_back( encryptCTR( (*it) , next(it) == plain.text.end() ) );
		++it;
	}
}
void blockCipher::decryptCpuCTR() {

	plain.text.clear();
	list<string>::iterator it = cipher.text.begin();
	while(it != cipher.text.end() ) {
		
		plain.text.push_back( decryptCTR(*it, next(it) == cipher.text.end() ) );
		++it;
	}
}
void blockCipher::encryptCpuECB() {

	cipher.text.clear();
	list<string>::iterator it = plain.text.begin();
	while(it != plain.text.end()) {

		cipher.text.push_back( encryptECB(*it, next(it) == plain.text.end() ) );
		++it;
	}
}
void blockCipher::decryptCpuECB() {

	plain.text.clear();
	list<string>::iterator it = cipher.text.begin();
	while(it != cipher.text.end()) {

		plain.text.push_back( decryptECB( *it, next(it) == cipher.text.end() ) );
		++it;
	}
}

void blockCipher::loadCipher(const string& path ) {

	cipher.loadFile(path);
}
void blockCipher::saveCipher(const string& path ) {

	cipher.saveFile(path);
}
void blockCipher::loadPlain(const string& path ) {

	plain.loadFile(path);
}
void blockCipher::savePlain(const string& path ) {
	
	plain.saveFile(path);
}

ostream &operator<<( ostream &stream, blockCipher &object) {
	
	list<string>::iterator
		ptIt = object.plain.text.begin(),
		ctIt = object.cipher.text.begin();

	for( ; ptIt != object.plain.text.end() && ctIt != object.cipher.text.end(); ++ptIt, ++ctIt )
		stream
		<< (*ptIt)
		<< "\t"
		<< (*ctIt)
		<< "\n";

	for( ; ptIt != object.plain.text.end(); ++ptIt )
		stream << (*ptIt) << "\n";

	for( ; ctIt != object.cipher.text.end(); ++ctIt )
		stream << "\t" << (*ctIt) << "\n";

	return stream;
}