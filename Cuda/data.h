#include "stdafx.h"
#include <list>
#include <fstream>
#include <string>
#include <iostream>

using namespace std;


class data {

	friend class blockCipher;
protected:

	list<string> text;

public:
	data();
	~data();

	//########## metody pracy na plikach
	void loadFile( const string&);
	void saveFile( const string&);

	//########## operatory
	friend ostream &operator<<(ostream&, data&);
	friend ostream &operator<<( ostream&, blockCipher&);
};