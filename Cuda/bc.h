#include "stdafx.h"

#include "..\CryptoPP\secblock.h"
	using CryptoPP::SecByteBlock;
#include "..\CryptoPP\osrng.h"
	using CryptoPP::AutoSeededRandomPool;
#include "..\CryptoPP\filters.h"
	using CryptoPP::StringSink;
	using CryptoPP::StringSource;
	using CryptoPP::StreamTransformationFilter;
#include "..\CryptoPP\aes.h"
	using CryptoPP::AES;
#include "..\CryptoPP\modes.h"
	using CryptoPP::CTR_Mode;
	using CryptoPP::ECB_Mode;

#include <string>
#include <iostream>
#include "data.h"
using namespace std;

class blockCipher {

protected:

	SecByteBlock key;
	SecByteBlock iv;

	data plain;
	data cipher;

	string encryptCTR( string&, bool);
	string decryptCTR( string&, bool);

	string encryptECB( string&, bool);
	string decryptECB( string&, bool);

public:

	blockCipher();
	~blockCipher();

	void encryptCpuCTR();
	void decryptCpuCTR();
	void encryptCpuECB();
	void decryptCpuECB();

	void loadPlain(const string&);
	void savePlain(const string&);

	void loadCipher(const string&);
	void saveCipher(const string&);

	friend ostream &operator<<( ostream&, blockCipher&);
};